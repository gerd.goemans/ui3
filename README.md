# verzorgingsapplicatie ziekenhuis (examen project)

## Gekozen keuzefunctionaliteit
Ik heb gekozen om de keuzefunctionaliteit g volledig uit te werken.  

### Gebruikers die in de databank zitten
De volgende gebruikers zitten in de databank:

Username		|	Wachtwoord	|	Roles
---------------------------------------------------------------------------
admin			|	password	|	Geregistreerde gebruiker, admin, dokter
Verpleegster	|	password	|	Geregistreerde gebruiker, verpleegster
Dokter			|	password	|	Geregistreerde gebruiker, dokter
bezoeker		|	password	|	Geregistreerde gebruiker

## security
Zoals vermeld heb ik gekozen voor keuzefunctionaliteit g, beveiliging met gebruikersbeheer.  

De wachtwoorden die gebruikers ingeven worden client side gesalt en gehashed.  Alleen de hash wordt over het internet doorgestuurd.  
De server kent het wachtwoord van de gebruiker niet.  En door de salt is het ook onmogelijk om te zien aan de hash dat verschillende 
gebruikers hetzelfde wachtwoord hebben zoals in de voorbeeld data het geval is.  Als salt heb ik gekozen voor de gebruikersnaam, meer 
hierover hieronder.

In verband met security moet ik hier een aantal dingen over zeggen:

1: Het algoritme gebruikt om de gesalte wachtwoorden te hashen, sha-256, is op de manier dat ik het gebruikt heb niet veilig genoeg 
	voor een echte website.  Het probleem is dat ik ervoor gekozen heb om als salt de gebruikersnaam te kiezen.  Dit wil zeggen dat 
	als een gebruiker een username en password combinatie gebruikt die redelijk veel voorkomend is, bijvoorbeeld: admin & password, 
	kan deze hash heel gemakkelijk opgezocht worden in een lookup table (wat is succesvol heb gedaan voor deze combinatie).  Een 
	veiliger alternatief zou een beter (en mogelijks minder gebruikt) hashing algoritme te gebruiken of om meer in de salt te steken 
	of om een random string in de salt te steken.
	
2: Vermits we in deze cursus ons focussen op het UI gedeelte en we dus geen backend hebben zit deze applicatie vol met security issues 
	en gebeuren er bij vele normale acties data leaks.  Dit laatste doordat er informatie over bijvoorbeeld andere gebruikers wordt 
	opgehaald van de databank bij het registreren van een nieuwe gebruiker.
	
3: Het token dat gebruikt wordt om een user te authentiseren is gewone random string.  Er zit niets van data in dit token.  Indien er 
	een backend zou zijn zou dit wel mogelijk zijn.  Ook kunnen tokens niet vervallen.  Ze hebben in de databank wel een vervaldatum 
	maar vermits er geen backend is worden deze tokens niet verwijderd uit de databank.  Ik zou dit kunnen controleren op de client 
	maar clientside validatie van dat soort dingen biedt geen echte meerwaarde.

##gitlab
Ik heb gebruik gemaakt van gitlab.  Mijn project staat in een private gitlab repo op de volgende locatie:
https://gitlab.com/gerd.goemans/ui3

## Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
