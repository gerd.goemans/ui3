
const routes = [
	{
		path: '/',
		component: () => import('layouts/RootLayout.vue'),
		children: [
			{ path: '', alias: ['/afdelingen', '/departments'], component: () => import('pages/departments.vue') },
			{ name: "department", props: true, path: '/department/:id', component: () => import('pages/department.vue') },
			{ name: "afdeling", props: true, path: '/afdeling/:id', component: () => import('pages/department.vue') },
			{ name: "patient", props: true, path: '/patient/:id', component: () => import('pages/patientInfo.vue') },
			{ path: '/cookies', component: () => import('pages/cookie-consent.vue') },
			{ path: '/login', component: () => import('pages/login.vue') },
			{ path: '/register', component: () => import('pages/register.vue') },
			{ path: '/resetpassword', component: () => import('pages/resetPassword.vue') },
			{ path: '/admin/usermanagment', component: () => import('pages/usermanagment.vue') },
			{ name: 'error404', path: 'error404', component: () => import('pages/Error404.vue') }
		]
	}
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
	routes.push({
		path: '*',
		redirect: 'error404'
	})
}

export default routes
